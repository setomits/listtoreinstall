package jp.straitmouth.listtoreinstall;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class ListToReinstallActivity extends Activity
    implements OnItemClickListener, View.OnClickListener {
    private static final int ONITEMCLICK = 1;
    
    private Handler mHandler = new Handler(){
	    public void handleMessage(android.os.Message msg) {
		switch (msg.what) {
		case ONITEMCLICK:
		    searchAtMarket(msg.arg1);
		    break;
		default:
		    break;
		}
	    };
        };

    private Button listButton;
    private ListView mAppListView;
    private List<String> mPackageNameList;
        
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

	listButton = (Button) findViewById(R.id.list_button);
	listButton.setOnClickListener (this);

        ListView listView = (ListView) findViewById(R.id.apps);
        mAppListView = listView;
        listView.setOnItemClickListener(this);

	setAppListView();
    }
    
    private void listInstalledApps() {
	try {
	    File fpath = new File(getString(R.string.file_path));
	    fpath.getParentFile().mkdirs();
	    BufferedWriter bw =
		new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fpath), "UTF-8"));

	    PackageManager pm = this.getPackageManager();	
	    List<ApplicationInfo> list =
		pm.getInstalledApplications(PackageManager.GET_META_DATA);

	    for (ApplicationInfo ai : list) {
		if ((ai.flags &
		     ApplicationInfo.FLAG_SYSTEM) == ApplicationInfo.FLAG_SYSTEM) {
		} else {
		    bw.write(ai.loadLabel(pm).toString() + "\t" +
			     ai.packageName + "\n");
		}
	    }

	    bw.close();
        } catch (IOException e) {
	    Toast.makeText(this, R.string.write_error, Toast.LENGTH_LONG).show();
	    Log.e(getString(R.string.app_name), e.toString());
        }

	setAppListView();

	listButton.setClickable(true);
    }

    private void setAppListView() {
	try {
	    ArrayAdapter<String> adapter = 
		new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
	    
	    if (mPackageNameList == null) {
		mPackageNameList = new ArrayList<String>();
	    } else {
		mPackageNameList.clear();
	    }

	    File fpath = new File(getString(R.string.file_path));
	    BufferedReader reader =
		new BufferedReader(new InputStreamReader(new FileInputStream(fpath), "UTF-8"));
	    
	    String line;
	    while((line = reader.readLine()) != null) {
		String[] splited = line.split("\t");
		adapter.add(splited[0]);
		mPackageNameList.add(splited[1]);
	    }
	    
	    mAppListView.setAdapter(adapter);
	} catch (UnsupportedEncodingException e) {
	    Toast.makeText(this, R.string.utf8_error, Toast.LENGTH_LONG).show();
	    Log.e(getString(R.string.app_name), e.toString());
	} catch (FileNotFoundException e) {
	    Log.i(getString(R.string.app_name), e.toString());
	} catch (IOException e) {
	    Toast.makeText(this, R.string.read_error, Toast.LENGTH_LONG).show();
	    Log.e(getString(R.string.app_name), e.toString());
	}
    }

    public void onItemClick(AdapterView<?> arg0, View arg1,
			    int position, long arg3) {
    	Message msg = mHandler.obtainMessage(ONITEMCLICK, position, 0);
    	mHandler.sendMessage(msg);
    }
        
    public void onClick(View view) {
	if (view == listButton) {
	    listButton.setClickable(false);
	    listInstalledApps();
	}
    }

    private void searchAtMarket(int position) {
	try {
	    String packageName = mPackageNameList.get(position);
	    Uri uri = Uri.parse("market://search?q=pname:" + packageName);
	    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
	    startActivity(intent);
	} catch (Exception e) {
	    Toast.makeText(this, R.string.market_error, Toast.LENGTH_LONG).show();
	    Log.e(getString(R.string.app_name), e.toString());
	}
    }

}
